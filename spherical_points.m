function points = spherical_points(radius, centre, n_points)
    n_count = 0;
    
    points = zeros(3, n_points);
    
%     a = 4 * pi * (radius ^ 2) / n_points;
    a = 4 * pi / n_points;
    d = sqrt(a);
    
    M_theta = round(pi/d);
    d_theta = pi / M_theta;
    d_phi = a / d_theta;
    
    for m = 0:(M_theta - 1)
       theta = pi * ( m + 0.5 ) / M_theta;
       M_phi = round(2 * pi * sin(theta) / d_phi);
       
       for n = 0:(M_phi - 1)
          phi = 2 * pi * n / M_phi;
          new_point = radius * [sin(theta) * cos(phi); sin(phi) * sin(theta); cos(theta)];
          n_count = n_count + 1;
          points(:, n_count) = new_point + centre;
       end
       
    end


end