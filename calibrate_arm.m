function calibrate_arm()

T_hand_camera_estimate = util.Rt2Transform(util.rotz(-2*pi/4), [0.095;-0.01;0.03]);
grid_size = 0.023; % size of grid squares in mm used for calibration

%Capture the images using the robotic arm
[calibration_images, T_base_hand] = camera_calibration.capture_calibration_images(...
    'plot_camera', true, ...
    'load_images', './calibration_images',...
    'T_hand_camera_estimate', T_hand_camera_estimate);

% Use MATLAB's camera calibration tool to generate the camera intrinsics
% and poses for each image
[cameraParams, images_used] = camera_calibration.calibrate_camera(....
    calibration_images, 'square_size', grid_size, 'plot', true);

% Filter out the images and poses from the robot that weren't used in calibration
calibration_images = calibration_images(:,:,:,images_used);
T_base_hand = T_base_hand(:, :, images_used);




% Extract poses corresponding to each image
R_camera_world = cameraParams.RotationMatrices;
t_camera_world = cameraParams.TranslationVectors;
% Convert the R and t matrix to an affine transform
T_camera_world = zeros(4,4,size(R_camera_world,3));
T_world_camera = zeros(4,4,size(R_camera_world,3));

for i=1:size(R_camera_world,3)
    T = eye(4);
    
    T(1:3, :) = [R_camera_world(:,:,i); t_camera_world(i,:)]';
    % Coordinates in the image frame are rotated to the coordinates in the
    % camera frame
    %T_camera_world(:,:,i) =  util.Rt2Transform(util.rotz(-pi/2), [0;0;0]) * T;
    T_camera_world(:,:,i) =  T;

    T_world_camera(:,:,i) = inv(T_camera_world(:,:,i));
end




% Perform the hand-eye coordination to determine the transformation between
% the robot hand and the optical center of the camera.
% T_hand_camera is the transformation from camera coordinates to hand
% coordinates

T_hand_camera = camera_calibration.handEye(T_base_hand, T_world_camera)

T_world_base = camera_calibration.get_world_base_transform(cameraParams, T_base_hand, T_hand_camera)

camera_calibration.reproject_grid_points(calibration_images, cameraParams, grid_size, T_world_base, T_base_hand, T_hand_camera);


figure
hold on;
title('Camera configuration after calibration (WRT Base)');
for i = 1:size(T_base_hand,3)
        
        T_base_hand_i = T_base_hand(:,:,i);
        util.plot_rgb_axes(T_base_hand_i, 'label', sprintf('Hand %d', i), 'scale', 0.02)

        
        T_base_camera =  T_base_hand_i * T_hand_camera;
        util.plot_camera(T_base_camera, 'Color', 'blue')
end



util.plot_rgb_axes(eye(4), 'label', 'Base', 'scale', 0.02);
util.plot_rgb_axes(inv(T_world_base), 'label', sprintf('World'), 'scale', 0.02)




save(sprintf("calibration_data/%s.mat", datestr(now, 'yyyy-mm-dd-HHMMSS')), 'T_hand_camera', 'T_base_hand', 'T_world_base', 'cameraParams', 'images_used');

end
