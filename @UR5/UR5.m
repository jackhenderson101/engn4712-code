classdef UR5 < handle
    properties
        connection
    end
    
    methods
        function this = UR5(ip_address, port)
            % Establishes a connection to the UR5 robot over TCP/IP
            % The UR5 must be running the polyscope program and awaiting a connection

            if nargin < 2
                port = 30000;
            end
            if nargin < 1
                ip_address = '192.168.100.1';
            end
            

            fprintf('Connecting to UR5 Robot on %s:%d\n', ip_address, port);
            this.connection = tcpip(ip_address, port, 'NetworkRole', 'server');
            fclose(this.connection);
            fprintf('Press "Play" on the robot...\n');
            fopen(this.connection);
            fprintf('Successfully connected to UR5\n');
        end
    end
    
end

