function move(ur5, T )
% Move the robot to a specified pose.
% T is a [4x4] transformation matrix relative to the robot base

[R,t] = util.Transform2Rt(T);
fprintf('Moving hand to %.2f, %.2f, %.2f', t(1), t(2), t(3));

R_axang = util.rotmat2axang(R);

P_char = ['(',num2str(t(1)),',',...
    num2str(t(2)),',',...
    num2str(t(3)),',',...
    num2str(R_axang(1)),',',...
    num2str(R_axang(2)),',',...
    num2str(R_axang(3)),...
    ')'];
success = '0';
while strcmp(success,'0')
    fprintf(ur5.connection,'(1)'); % task = 1 : moving task
    pause(0.01);% Tune this to meet your system
    fprintf(ur5.connection, P_char);
    while ur5.connection.BytesAvailable==0
        %t.BytesAvailable
    end
    success  = ur5.read_msg();
end
if ~strcmp(success,'1')
    error('error sending robot pose')
end
fprintf('Move command sent\n');
% wait for the robot to reach the desired pose
distance = Inf;
threshold = 0.0001;
while distance > threshold
    pause(0.1);
    current_pose = ur5.read_pose();
    % inv(T) * current_pose should be close to the identity, as they should
    % be almost equal
    distance = norm(T\current_pose - eye(4));
    fprintf('Current distance from target pose: %.4f\n', distance);
end

end

