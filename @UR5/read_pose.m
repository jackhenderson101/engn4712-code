function [ T ] = read_pose( ur5 )
% Read the current pose of the robot and return as a [4x4] transformation
% matrix, representing the pose of the robot relative to the base.
    
% Read out unwanted bytes
if ur5.connection.BytesAvailable>0
    fscanf(ur5.connection,'%c', ur5.connection.BytesAvailable);
end

fprintf(ur5.connection,'(2)'); % task = 2 : reading task

% Wait for response
while ur5.connection.BytesAvailable==0
end

rec = fscanf(ur5.connection,'%c',ur5.connection.BytesAvailable);
if ~strcmp(rec(1),'p') || ~strcmp(rec(end),']')
    error('robotpose read error')
end
rec(end) = ',';
Curr_c = 2;
for i = 1 : 6
    C = [];
    Done = 0;
    while(Done == 0)
        Curr_c = Curr_c + 1;
        if strcmp(rec(Curr_c) , ',')
            Done = 1;
        else
            C = [C,rec(Curr_c)];
        end
    end
    pose(i) = str2double(C);   
end
for i = 1 : length(pose)
    if isnan(pose(i))
        error('robotpose read error (Nan)')
    end
end

% Robot returns pose in [1x6] vector, representing x,y,z and rotation in
% axis-angle form. Convert it to a [4x4] transformation matrix.

R = util.axang2rotmat(pose(4:6));
T = util.Rt2Transform(R, pose(1:3)');
end

