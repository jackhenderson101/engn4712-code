classdef Camera < handle
    %CAMERA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        video_object
        width
        height
    end
    
    methods
        function c = Camera(device, id)
            if nargin < 1
                info = imaqhwinfo();
                info = imaqhwinfo(info.InstalledAdaptors{1});
                if isempty(info.DeviceIDs)
                    error('No video devices found for adaptor %s', info.AdaptorName);
                end
                device = info.AdaptorName;
                fprintf("Using default video device: %s\n", device); 
            end
            if nargin < 2
                id = 1;
            end
            c.video_object = videoinput(device, id);
            triggerconfig(c.video_object, 'manual');
            start(c.video_object);
            
            props = get(c.video_object);
            
            c.width = props.VideoResolution(1);
            if c.width == 2560
                c.width = 1280;
            end
            
            c.height = props.VideoResolution(2);
            
            fprintf("Successfully connected to camera\n");
            fprintf("Resolution: %d x %d\n", c.width, c.height);
        end
        
        function delete(this)
            if ~isempty(this.video_object)
                fprintf('Stopping video capture from camera\n');
                flushdata(this.video_object);
                stop(this.video_object); 
            end
            imaqreset;
        end
        
        function show_preview(this)
           preview(this.video_object); 
        end
        
        function image = capture_image(this)
            image = getsnapshot(this.video_object);
            if this.video_object.ReturnedColorSpace == 'YCbCr'
                image = ycbcr2rgb(image);
            end
            % Only use one half of the ZED camera image
            image = image(:,1:this.width, :); 
        end

    end
    
end

