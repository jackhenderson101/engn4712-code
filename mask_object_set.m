function mask_object_set(object_name, dataset_path)

figure
for i=1:50
    
    object = imread(sprintf('%s/%s%d.jpg', dataset_path, object_name, i));
    background = imread(sprintf('%s/blank%d.jpg', dataset_path, i));
    
    [mask, masked_image] = data_processing.mask_object(object, background, 'threshold', 120, 'shadow_threshold', 0.5);
    
    imshow(masked_image);
    drawnow;


end