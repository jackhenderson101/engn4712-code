function [ output_args ] = show_ground_truth( T_world_base, T_base_hand, T_hand_camera, cameraParams, images_dir, object_name, object_dimensions, object_position )
    
for i = 1:size(T_base_hand,3)
    clf
    image = imread(sprintf('%s/%s%d.jpg', images_dir, object_name, i));
    image = undistortImage(image, cameraParams);
    imshow(image);
    T_camera_world =  inv(T_world_base * T_base_hand(:,:,i)*T_hand_camera) * util.Rt2Transform(eye(3), object_position);
    
    util.plot_bounding_box(object_dimensions, T_camera_world, cameraParams);
    
    drawnow;
   pause(0.3);
end



end

