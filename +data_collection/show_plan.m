function [ output_args ] = show_plan( T_world_base, T_base_hand, T_hand_camera )

    figure()
    grid on
    xlabel('X (m)')
    ylabel('Y (m)')
    zlabel('Z (m)')
    title('Imaging Plan');
    util.plot_rgb_axes(eye(4), 'label', 'World', 'scale', 0.03);
    %util.plot_rgb_axes(T_world_base, 'label', 'Base', 'scale', 0.02);
    
    for i = 1:size(T_base_hand,3)
       T_world_hand = T_world_base * T_base_hand(:,:,i);
       %util.plot_rgb_axes(T_world_hand, 'label', sprintf('H_{%d}', i), 'scale', 0.01);
       T_world_camera = T_world_hand * T_hand_camera;
       util.plot_camera(T_world_camera, 'Label', sprintf('C_{%d}', i), 'AxesVisible', false);
    end


end

