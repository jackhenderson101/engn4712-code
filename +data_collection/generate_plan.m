function [ T_base_hand_near, T_base_hand_far ] = generate_plan( varargin )
    
p = inputParser;
addParameter(p, 'sample_method', 'uniform_spherical');
addParameter(p, 'camera_focus', [0;0;0]); % Where the cameras should point, in the world frame
addParameter(p, 'T_world_base', eye(4));
addParameter(p, 'T_hand_camera', eye(4));
addParameter(p, 'depths', []);
addParameter(p, 'density', 100);
addParameter(p, 'min_height', 0.1);
addParameter(p, 'calibration', false);
parse(p, varargin{:});


camera_positions = [];

if p.Results.calibration
    heights = 0.2:0.05:0.6;
    for h = heights
        camera_positions = [camera_positions, [-0.1;-0.1;h]];
    end
end


for d=p.Results.depths
    camera_positions = [camera_positions, spherical_points(d, [0;0;0], p.Results.density * 2)];
end



% Keep only the positions above the minimum height
camera_positions = camera_positions(:, camera_positions(3,:) > p.Results.min_height);

% split hemisphere into a near and a far set
near_half = camera_positions(1,:) <= 0;

% rotate the far half of points 180 degrees
camera_positions(:,~near_half) = util.rotz(pi) * camera_positions(:,~near_half);

camera_positions = util.rotz(0.12*pi) * camera_positions;

camera_positions_world = camera_positions + p.Results.camera_focus;
num_positions = size(camera_positions,2);
T_base_hand = zeros(4,4, num_positions);
 
    T_flip = util.Rt2Transform(util.rotx(pi));
    T_rotate = util.Rt2Transform(util.rotz(-pi/2));

for i = 1:num_positions
    translation = camera_positions_world(:, i);
    rotation = rotate_towards_point(translation, p.Results.camera_focus);
    T_world_camera = T_flip * util.Rt2Transform(rotation, translation) * T_rotate;
    

    T_base_hand(:,:,i) = inv(p.Results.T_world_base) * T_world_camera * inv(p.Results.T_hand_camera) ;
end

T_base_hand_near = T_base_hand(:,:,near_half);
T_base_hand_far = T_base_hand(:,:,~near_half);

end

