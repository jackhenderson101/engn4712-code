function [ img ] = capture_single_view( ur5, camera, pose, filename )
    % Move the robot to the required pose, capture an image and save it.
    
    ur5.move(pose);
    pause(0.5);
    img = camera.capture_image();
    
    if nargin > 3
        imwrite(img, filename);
    end
end

