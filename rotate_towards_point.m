function [ R ] = rotate_towards_point( camera, focus )
% Generate a rotation matrix so that the camera is pointing towards the
% focus, and the orientation of the camera is upright.
% i.e. the z-axis of the camera points to the focus and the y-axis is
% parallel to the horizon (world x-y plane).

vector_to_focus = focus - camera;
pitch_depth = sqrt(vector_to_focus(1)^2 + vector_to_focus(2)^2);
pitch_angle = atan(vector_to_focus(3)/pitch_depth);
yaw_angle = atan(vector_to_focus(2)/vector_to_focus(1));
if vector_to_focus(1) < 0
    yaw_angle = yaw_angle + pi;
end
if isnan(yaw_angle)
    % Can occur when x and y are zero
    yaw_angle = 0;
end

R_xz = util.roty(pi/2); % rotate so the z-axis now points in the x direction
R_pitch = util.roty(-pitch_angle);
R_yaw = util.rotz(yaw_angle);

% Put the camera upside-down, so the robot doesn't collide with the camera
% mount
R_roll = util.rotx(pi);

R = R_yaw * R_pitch * R_roll * R_xz;


%%%% Testing
% pts = [0,0,0; eye(3)]';
% new_pts = R*pts;
% figure
% hold on
% plot3(new_pts(1,1:2), new_pts(2,1:2), new_pts(3,1:2), 'r');
% plot3(new_pts(1,[1,3]), new_pts(2,[1,3]), new_pts(3,[1,3]), 'g');
% plot3(new_pts(1,[1,4]), new_pts(2,[1,4]), new_pts(3,[1,4]), 'b');
% plot3(focus(1), focus(2), focus(3), 'k*');
% axis equal
end

