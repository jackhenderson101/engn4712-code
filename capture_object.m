function [ output_args ] = capture_object(camera, varargin)
    

p = inputParser;
addParameter(p, 'calibration_file', '');
addParameter(p, 'name', 'test');
addParameter(p, 'sample_method', "uniform_spherical");
addParameter(p, 'camera_source', 'macvideo');
addParameter(p, 'camera_id', 2);
addParameter(p, 'camera_depths', [0.3]);
addParameter(p, 'show_camera', false);
addParameter(p, 'plot_poses', true);
addParameter(p, 'plot_camera', true);
parse(p, varargin{:});


% load the camera intrinsics and the hand-camera transformation.
load(p.Results.calibration_file)

% TODO dataset naming system
base_path = sprintf('data/%s', p.Results.name);

camera_focus = 0.023 * [4;-2;0];


[poses_near, poses_far] = data_collection.generate_plan(...
    'T_world_base', T_world_base, ...
    'T_hand_camera', T_hand_camera, ...
    'depths', p.Results.camera_depths,...
    'min_height', 0.15, ...
    'density', 200, ...
    'camera_focus', camera_focus ...
);

if p.Results.plot_poses
    data_collection.show_plan(T_world_base, poses_near, T_hand_camera);
    drawnow
end

T_base_hand = poses_near;
save('object_capture_poses.mat', 'T_base_hand', 'T_world_base');

ur5 = UR5();
% camera = Camera(p.Results.camera_source, p.Results.camera_id);

images = uint8([]);
if p.Results.plot_camera
    image_fig = figure();
end

for i = 1:size(poses_near,3)
   
   filename = sprintf('%s%d.jpg', base_path, i);
   images(:,:,:,i) = data_collection.capture_single_view(ur5, camera, poses_near(:,:,i), filename);
   
   
    if p.Results.plot_camera
        figure(image_fig);
        imshow(images(:,:,:,i));
        hold on
        plot(cameraParams.PrincipalPoint(1), cameraParams.PrincipalPoint(2), 'r*')
    end
   %TODO: Save pose information in standard format - need to see what data
   %already exists.
   
   
   %TODO Prompt to spin object 180 degrees around and capture the other
   %side
   
   %TODO plot view of scene
   
    
    
end




end

