function [ images, poses ] = capture_calibration_images(varargin)
    % Positions the robot at various locations with the camera pointed
    % towards the centre of the workspace and captures an image at each
    % location.
    
    p = inputParser;
    addParameter(p, 'plot_camera', false);
    addParameter(p, 'plot_poses', true);
    addParameter(p, 'camera_source', 'macvideo');
    addParameter(p, 'camera_id', 2);
    addParameter(p, 'load_images', ""); % Specify a folder to load pre-captured calibration images from
    addParameter(p, 'camera_focus', [0;0;0]);
    addParameter(p, 'T_hand_camera_estimate', eye(4)); % Initial estimate of the T_hand_camera matrix
    parse(p, varargin{:});
    
    T_world_base = util.Rt2Transform(util.rotz(pi/4), [-0.7;0;0.2]);
    
    [poses, ~] = data_collection.generate_plan(...
        'camera_focus', p.Results.camera_focus,...
        'T_world_base', T_world_base, ...
        'T_hand_camera', p.Results.T_hand_camera_estimate, ...
        'depths', [0.25 0.3 0.35], ...
        'density', 100, ...
        'min_height', 0.15, ...
        'calibration', true ...
    );

    images = uint8([]); % zeros(0, 0, 3, num_positions, 'uint8');
    
    poses = poses(:,:,1:74);
    
    if p.Results.plot_poses
        data_collection.show_plan(T_world_base, poses, p.Results.T_hand_camera_estimate);
        drawnow
    end
    
    
    % execute the plan
    
    if p.Results.load_images == ""
        % We don't need the camera or the robot if we load the images from
        % folder
        robot = UR5();
        camera = Camera2('ZED');       
    end
    
    num_positions = size(poses,3);
    if p.Results.plot_camera
        image_fig = figure();
    end
    
    for i = 1:num_positions
        if p.Results.load_images == ""
            fprintf('Capturing calibration image %d of %d\n', i, num_positions);
            images(:,:,:,i) = data_collection.capture_single_view(robot, camera, poses(:,:,i), sprintf('calibration_images/%d.jpg', i));
        else
            fprintf('Loading calibration image %d of %d\n', i, num_positions);
            images(:,:,:,i) = imread(sprintf('%s/%d.jpg', p.Results.load_images, i));
        end

        if p.Results.plot_camera
            figure(image_fig);
            imshow(images(:,:,:,i));
            hold on
            plot(1280/2, 720/2, 'r*')
        end
       
    end


end

