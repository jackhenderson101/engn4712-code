function [ T_world_base_mean ] = get_world_base_transform( cameraParams, T_base_hand, T_hand_camera )
    T_camera_world = util.get_transform_from_camera_params(cameraParams);
    

    assert(all(size(T_base_hand) == size(T_camera_world)));

    figure
    title('Base coordinate frame relative to world frame')
    
    T_world_base = zeros(size(T_camera_world));
    
%     T_mount_camera = util.Rt2Transform(util.rotz(-pi/2), [0;0;0]);    
    
    for i = 1: size(T_world_base,3)
       T_world_base(:,:,i) =  inv(T_base_hand(:,:,i) * T_hand_camera * T_camera_world(:,:,i));
       
       util.plot_rgb_axes(T_world_base(:,:,i), 'scale', 0.01);
    end
    
    R_mean = mean(T_world_base(1:3,1:3,:),3);
    T_mean = mean(T_world_base(1:3,4,:),3);
    
    [u,s,v] = svd(R_mean);
    % Make the eienvalues 1
    R_mean = u * eye(3) * v';
    
    T_world_base_mean = util.Rt2Transform(R_mean, T_mean);
    util.plot_rgb_axes(T_world_base_mean, 'scale', 0.02);

    
    


end

