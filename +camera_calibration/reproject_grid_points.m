function reproject_grid_points( images, cameraParams, grid_size,  T_world_base, T_base_hand, T_hand_camera)
    T_camera_world = util.get_transform_from_camera_params(cameraParams);
    K = cameraParams.IntrinsicMatrix';
    distortion = cameraParams.RadialDistortion;
    projection = [eye(3);0,0,0]';

    
    projection_img = figure();
    
    for i=1:size(images,4)
        
        T_camera_world_i = T_camera_world(:,:,i);        
        
        figure(projection_img);
        clf;
        imshow(images(:,:,:,i));
        hold on
        
        for x = -1:9
            for y = -1:6
                P_world = [x * grid_size; y * grid_size; 0; 1];
                P_camera = T_camera_world_i * P_world;
                
                P_2_camera = inv(T_world_base * T_base_hand(:,:,i) * T_hand_camera) * P_world;
                


                P_image = K * projection * P_camera;
                P_2_image = K * projection * P_2_camera;
                % De-homogenise the coordinates.
                P_image = P_image ./ P_image(3);
                P_2_image = P_2_image ./ P_2_image(3);
                
                P_image_normalised = (P_image(1:2) - cameraParams.PrincipalPoint')./ cameraParams.FocalLength';
                
                
                r2 = sum(P_image_normalised.^2);
                P_image_distorted = P_image_normalised .* (1 + distortion(1) * r2 + distortion(2) * r2^2);
                P_image_distorted = P_image_distorted .* cameraParams.FocalLength' + cameraParams.PrincipalPoint';
                undistorted_point =  K * projection *  P_camera;
                undistorted_point = distortPoints(cameraParams, P_image(1:2)');
                %undistorted_point = undistorted_point ./ undistorted_point(3);
                plot(P_image(1), P_image(2), 'b*');
                plot(P_2_image(1), P_2_image(2), 'ys');
                plot(P_image_distorted(1), P_image_distorted(2), 'ro');

            end
        end
        
        drawnow;
    end

    
end