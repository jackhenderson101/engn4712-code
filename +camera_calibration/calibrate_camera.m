function [ cameraParams, images_used ] = calibrate_camera(images, varargin)
    % Calibtrate the camera intrinsics based on a collection of images of a
    % checkerboard pattern
    
    p = inputParser;
    addParameter(p, 'square_size', 0.0128);
    addParameter(p, 'plot', false);
    parse(p, varargin{:});
    
    square_size = p.Results.square_size;
    
    % Detect checkerboards in images
    fprintf('Detecting Checkerboard points in images...');
    [imagePoints, boardSize, cb_images_used] = detectCheckerboardPoints(images);
    
    fprintf('Checkerboard points detected in %d of %d images.\n', sum(cb_images_used), length(cb_images_used));
    fprintf('Using square size of %.2f mm\n', square_size*1000);

    [mrows, ncols, ~, ~] = size(images);

    % Generate world coordinates of the corners of the squares
    worldPoints = generateCheckerboardPoints(boardSize, square_size);

    % Calibrate the camera
    [cameraParams, cal_images_used, estimationErrors] = estimateCameraParameters(imagePoints, worldPoints, ...
        'EstimateSkew', false, 'EstimateTangentialDistortion', false, ...
        'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'meters', ...
        'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
        'ImageSize', [mrows, ncols]);
    
    fprintf('Calibration Complete. Used %d of %d available images for calibration\n', sum(cal_images_used), length(cal_images_used));

    % View reprojection errors
    % h1=figure; showReprojectionErrors(cameraParams);

    % Visualize pattern locations
    if p.Results.plot
        h2=figure; showExtrinsics(cameraParams, 'PatternCentric');
    end

    % Display parameter estimation errors
    displayErrors(estimationErrors, cameraParams);
    
    images_used = cb_images_used;
    images_used(images_used == 1) = cal_images_used;
end

