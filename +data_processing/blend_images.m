function [ blended_image ] = blend_images(background, foreground, mask )
    assert(all(size(background) == size(foreground)));
    assert(size(mask,1) == size(foreground,1) && size(mask,2) == size(foreground,2));

    mask3 = repmat(mask, 1, 1, 3);
    
    blended_image = uint8(mask3 .* double(foreground) + (1-mask3) .* double(background));

end

