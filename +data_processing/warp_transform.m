function [ warped_image ] = warp_transform(image, rotation, cameraParams)

    D = zeros(size(image,1), size(image,2), 2);
    K = cameraParams.IntrinsicMatrix';

    transform = K * rotation * inv(K);
    
    for y = 1:size(D,1)
        for x = 1:size(D,2)
            P_1 = [x;y;1];
            P_2 = (transform) * P_1;
            P_2 = P_2./P_2(3);
            D(y,x,:) = - ([x;y] - P_2([1,2]));
        end
    end
    

    
    figure

    warped_image = imwarp(image, D);
    imshow(warped_image);

end

