function [ mask, masked_image ] = mask_object( fg, bg, varargin )
    p = inputParser;
    addParameter(p, 'plot', false);
    addParameter(p, 'threshold', 200);
    addParameter(p, 'shadow_threshold', 0.1);
    parse(p, varargin{:});
    
    mask_ocv = ocv_mask_bg(fg, bg, p.Results.threshold, p.Results.shadow_threshold);
    
    mask_obj = mask_ocv==255;
    mask_shadow = mask_ocv == 127;
    
    mask_obj = imfill(mask_obj, 'holes');

    connected_components = bwconncomp(mask_obj);
    labels = labelmatrix(connected_components);
    object_label = labels(end/2+30, end/2);
    mask = (labels == object_label);
    
%     mask = imclose(mask, strel('disk', 5));
    
    
    masked_image = fg;
    masked_image(~repmat(mask,1,1,3)) = 255;
    
    if p.Results.plot
        figure
        subplot(2,2,1);
        imshow(mask_ocv);
        subplot(2,2,2);
        %imshow(edges);
        subplot(2,2,3);
        imshow(mask);
        subplot(2,2,4);
        imshow(masked_image);
    end
end
% 
%     image_hsv = rgb2hsv(image);
%     
%     hue_min = 0.580;
%     hue_max = 0.635;
%     
%     % Threshold the image based on hue values.
%     mask_hue = image_hsv(:,:,1) < hue_min | image_hsv(:,:,1) > hue_max; % .* image_hsv(:,:,1) < hue_max);
%     
%     % Dilate the binary image to remove any small gaps
%     SE = strel('diamond', 2);
%     mask_dilated = imdilate(mask_hue, SE);
%     
%     % Fill in any holes in the mask
%     mask_filled = imfill(mask_dilated, 'holes');
%     
%     % Calculate connected components in the image
%     connected_components = bwconncomp(mask_filled);
%     
%     % Find the connected component that is in the center of the image.
%     labels = labelmatrix(connected_components);
%     object_label = labels(end/2, end/2);
%     
%     % Isolate this connected comonent from the rest of the image
%     mask_isolated = labels == object_label;
%     
%     % Erode the mask to reduce the border
%     mask_eroded = imerode(mask_isolated, strel('sphere', 4));
%     
%     % Blur the border to produce a smooth blending between the image and
%     % the background.
%     mask_blurred = imgaussfilt(double(mask_eroded), 2);
%     mask = mask_eroded;
%     
% 
% 
% end

