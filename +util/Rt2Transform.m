function [ T ] = Rt2Transform( R, t )
% Convert a set of 3x3 Rotation matrix and a 3x1 translation to a 4x4 homogenous
% transform matrix
if nargin < 2
    t = [0;0;0];
end

num_transforms = size(t,3);

assert(size(R,1) == 3 && size(R,2) == 3)
assert(size(t,1) == 3 && size(t,2) == 1)
assert(size(R,3) == num_transforms)

% Extend R to a 4x4 matrix
T_rotation = repmat(eye(4),1,1,size(R,3));
T_rotation(1:3,1:3,:) = R;

T_translation = repmat(eye(4),1,1,size(t,3));
T_translation(1:3,4,:) = t;

T = zeros(4,4,num_transforms);
for i=1:num_transforms
    T(:,:,i) = T_translation(:,:,i) * T_rotation(:,:,i);
end

end

