function [ output_args ] = plot_rgb_axes_2d( T_camera_world, cameraParams, varargin )
%PLOT_RGB_AXES Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addParameter(p, 'scale', 1);
addParameter(p, 'label', '');
addParameter(p, 'distortion', false);
parse(p, varargin{:});
scale = p.Results.scale;

K = cameraParams.IntrinsicMatrix';
K(:,4) = [0;0;0];

origin = K*T_camera_world*[0;0;0;1];
x_axis = K*T_camera_world*[scale;0;0;1];
y_axis = K*T_camera_world*[0;scale;0;1];
z_axis = K*T_camera_world*[0;0;scale;1];

origin = origin./origin(3);
x_axis = x_axis./x_axis(3);
y_axis = y_axis./y_axis(3);
z_axis = z_axis./z_axis(3);

hold on
plot([origin(1),x_axis(1)], [origin(2),x_axis(2)], 'r');
plot([origin(1),y_axis(1)], [origin(2),y_axis(2)], 'g');
plot([origin(1),z_axis(1)], [origin(2),z_axis(2)], 'b');
end

