function [ R, t ] = Transform2Rt( T )
% Decompose the 4x4 Transformation matrix, T, into a 3x3 Rotation matrix and a
% 3x1 translation, R and t.
util.validate_transform(T);

% Ensure the matrix is normalised
T = T ./ T(4,4);

R = T(1:3,1:3);

T_rotation = eye(4);
T_rotation(1:3,1:3) = R;

t = T * T_rotation';
t = t(1:3,4);

end

