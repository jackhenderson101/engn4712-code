function [ output_args ] = plot_bounding_box( dimensions, T_camera_world, cameraParams )
P_world = zeros(4,8);
P_camera = zeros(4,8);
P_img = zeros(3,8);
P_world(4,:) = 1;
K = cameraParams.IntrinsicMatrix';
hold on

i = 1;
    for x = [-1,1]
        for y = [-1,1]
            for z = [-2,0]
                P_world(1:3,i) = 0.5 * dimensions .* [x;y;z];
                P_camera(:,i) = T_camera_world * P_world(:,i);
                P_img(:,i) = K * [eye(3);0,0,0]' * P_camera(:,i);
                P_img(:,i) = P_img(:,i) ./ P_img(3,i);
                %plot(P_img(1,i), P_img(2,i), 'ko');
                i = i+1;
            end
        end
    end
    
    [p,idx] = max(P_camera(3,:));
    
    P_img(:,idx) = NaN;
    
    
    connections = [
        0,0,0,0,0,0,0,0;
        1,0,0,0,0,0,0,0;
        1,0,0,0,0,0,0,0;
        0,1,1,0,0,0,0,0;
        1,0,0,0,0,0,0,0;
        0,1,0,0,1,0,0,0;
        0,0,1,0,1,0,0,0;
        0,0,0,1,0,1,1,0;
        ];
    for i = 1:8
        for j = 1:8
            if connections(i,j)
                P1 = P_img(:,i);
                P2 = P_img(:,j);
                plot([P1(1), P2(1)], [P1(2),P2(2)], 'r-', 'LineWidth', 3);
            end
        end
    end
    
    
    
    
    


end

