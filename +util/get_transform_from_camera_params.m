function [ T_camera_world ] = get_transform_from_camera_params( cameraParams )

    R_camera_world = cameraParams.RotationMatrices;
    t_camera_world = cameraParams.TranslationVectors;
    
    T_camera_world = zeros(4,4,size(R_camera_world,3));
    
    for i=1:size(T_camera_world,3)
        
        T_camera_world(:,:,i) = eye(4);
        T_camera_world(1:3, :, i) = [R_camera_world(:,:,i); t_camera_world(i,:)]';
    end

end

