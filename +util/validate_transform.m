function [ output_args ] = validate_transform( T )
% Ensure that the transformation T is a valid affine transform
assert(all(size(T) == [4,4]), 'Transformation is incorrect dimension');
assert(all(T(4,1:3) == 0), 'Transformation is not affine');
assert(T(4,4) == 1, 'Transformation not valid');

epsilon = 1e-12;
R = T(1:3,1:3)./T(4,4);
assert(all(all(abs(R * R' - eye(3)) < epsilon)) && abs(det(R) - 1) < epsilon, 'Rotation matrix is invalid')
end

