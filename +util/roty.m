function [ rotation ] = roty( angle )
% generate rotation matrix corresponding to a rotation about the y axis of
% 'angle' radians

rotation = [...
    cos(angle) 0 sin(angle);
    0 1 0;
    -sin(angle) 0 cos(angle);
    ];


end

