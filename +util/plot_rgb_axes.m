function [ output_args ] = plot_rgb_axes( T, varargin )
%PLOT_RGB_AXES Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addParameter(p, 'scale', 1);
addParameter(p, 'label', '');
addParameter(p, 'linewidth', 1);
parse(p, varargin{:});
scale = p.Results.scale;

origin = T*[0;0;0;1];
x_axis = T*[scale;0;0;1];
y_axis = T*[0;scale;0;1];
z_axis = T*[0;0;scale;1];
hold on
plot3([origin(1),x_axis(1)], [origin(2),x_axis(2)], [origin(3),x_axis(3)], 'r', 'LineWidth', p.Results.linewidth);
plot3([origin(1),y_axis(1)], [origin(2),y_axis(2)], [origin(3),y_axis(3)], 'g', 'LineWidth', p.Results.linewidth);
plot3([origin(1),z_axis(1)], [origin(2),z_axis(2)], [origin(3),z_axis(3)], 'b', 'LineWidth', p.Results.linewidth);

text(x_axis(1), x_axis(2), x_axis(3), p.Results.label);
axis equal

end

