function [ rotation ] = rotz( angle )
% generate rotation matrix corresponding to a rotation about the z axis of
% 'angle' radians

rotation = [...
    cos(angle) -sin(angle) 0;
    sin(angle) cos(angle) 0;
    0 0 1
    ];


end

