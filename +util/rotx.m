function [ rotation ] = rotx( angle )
% generate rotation matrix corresponding to a rotation about the y axis of
% 'angle' radians

rotation = [...
    1 0 0;
    0 cos(angle) -sin(angle);
    0 sin(angle) cos(angle);
    ];


end

