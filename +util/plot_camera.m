function plot_camera( T, varargin )

    [r, t] = util.Transform2Rt(T);

    % Matlab's represents rotations as a post-multiplication,
    % so we invert the rotation matrix
    r = inv(r);
    
    plotCamera('Location', t, 'Orientation', r, 'AxesVisible', true, 'Size', 0.01, varargin{:})


end

