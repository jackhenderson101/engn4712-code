classdef Camera2 < handle

    
    properties
        video_object
        width
        height
    end
    
    methods
        function c = Camera2(name)
            
            c.video_object = webcam(name);
          
            
            c.width = 1280;
          
            c.height = 720;
            
            fprintf("Successfully connected to camera\n");
            fprintf("Resolution: %d x %d\n", c.width, c.height);
        end
        
        function delete(this)
            
        end
        
        function show_preview(this)
        end
        
        function image = capture_image(this)
            image = snapshot(this.video_object);
            
            % Only use one half of the ZED camera image
            image = image(:,1:this.width, :); 
        end

    end
    
end

