hand_positions = [...
    0,0,0.3;
    0,0,0.4;
    0,0,0.5;
    -0.1,0.1,0.3;
    -0.1,0.1,0.4;
    -0.1,0.1,0.5;
    -0.2,0,0.3;
    -0.2,0,0.35;
    -0.2,0,0.4;
    0,0.2,0.3;
    0,0.2,0.35;
    0,0.2,0.4;
]';

camera_focus = [0;0;0];

T_hand_eye = util.Rt2Transform(util.rotz(-3*pi/4), [0.09;0.00;0.02]);

T_world_base = util.Rt2Transform(util.rotz(-pi/4), [-0.3;0.3;0]);

T_base_hand = [];
T_world_camera = [];
num_positions = size(hand_positions, 2);

figure; hold on;
xlabel('X')
ylabel('Y')
zlabel('Z')
plot3(camera_focus(1), camera_focus(2), camera_focus(3), 'b*');
for i = 1:num_positions
        translation = hand_positions(:, i);
        rotation = rotate_towards_point(translation, camera_focus);
        T_base_hand(:,:,i) = util.Rt2Transform(rotation, translation);
        
        T_world_camera(:,:,i) =  T_world_base * T_base_hand(:,:,i) * T_hand_eye  + 0.01*(rand(4)-0.5).* [0,0,0,1;0,0,0,1;0,0,0,1;0,0,0,0];
        
        util.plot_camera(T_world_camera(:,:,i), 'Color', 'blue')
        util.plot_rgb_axes(T_world_base * T_base_hand(:,:,i), 'label', sprintf('Hand %d', i), 'scale', 0.02)
end

util.plot_rgb_axes(eye(4), 'label', 'World', 'scale', 0.02);
util.plot_rgb_axes(T_world_base, 'label', 'Base', 'scale', 0.02);

T_hand_eye_cal = camera_calibration.handEye(T_base_hand, T_world_camera)

norm(inv(T_hand_eye_cal) * T_hand_eye - eye(4))

